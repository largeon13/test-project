const express = require("express");
const usersRouter = express.Router();
const usersController = require("../controllers/usersController");

/* GET users listing. */
usersRouter.get("/", usersController.getUsers);

usersRouter.post("/", usersController.addUser);

module.exports = usersRouter;
