const express = require("express");
const lessonsRouter = express.Router();
const lessonsController = require("../controllers/lessonsController");

lessonsRouter.get("/", lessonsController.getLessons);

lessonsRouter.post("/", lessonsController.addLesson);

lessonsRouter.post("/:id/evaluations", lessonsController.addEvalution);

module.exports = lessonsRouter;
