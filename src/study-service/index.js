var createError = require("http-errors");
var express = require("express");

var usersRouter = require("./routes/users");
var lessonsRouter = require("./routes/lessons");
const swaggerDoc = require("./swagger.json");
const swaggerUi = require("swagger-ui-express");

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api/user", usersRouter);
app.use("/api/lessons", lessonsRouter);

app.use("/docs", swaggerUi.serve);
app.use("/docs", swaggerUi.setup(swaggerDoc));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

app.listen(8080, () => {
  console.log("Listening on port 8080");
});

module.exports = app;
