const db = require("@lib/db");
const validation = require("validator");

exports.addLesson = async function (req, res) {
  try {
    let resultValidation = await validation.validateData(req.body, "addLesson");
    if (!resultValidation.success) {
      return res.send({
        code: "VALIDATION_ERROR",
        message: resultValidation.error,
      });
    }
    let lesson_exist = await db.lesson.findOne({
      where: { code: req.body.code },
    });
    if (lesson_exist) {
      return res.send({
        code: "LESSON_ALREADY_EXISTS",
        message: "A lesson with this code already exists",
      });
    }
    let lesson = await db.lesson
      .create({
        name: req.body.name,
        code: req.body.code,
      })
      .catch((err) => {
        res.send({ code: "CREATION_ERROR", message: err.name });
      });
    return res.send(lesson);
  } catch (err) {
    res.send({ code: err.name, message: err.message });
  }
};

exports.addEvalution = async function (req, res) {
  req.body.lesson_id = req.params.id;
  try {
    let resultValidation = await validation.validateData(
      req.body,
      "addEvalution"
    );
    if (!resultValidation.success) {
      return res.send({
        code: "VALIDATION_ERROR",
        message: resultValidation.error,
      });
    }
    if (
      isNaN(req.body.score) ||
      isNaN(req.body.user_id) ||
      isNaN(req.body.lesson_id)
    ) {
      return res.send({
        code: "INVALID_DATA",
        message: "score, user_id and lesson_id must be a number",
      });
    }
    let user = await findUser(req.body.user_id);
    if (!user)
      return res.send({
        code: "USER_NOT_FOUND",
        message: "User not found",
      });

    let lesson = await findLesson(req.body.lesson_id);
    if (!lesson)
      return res.send({
        code: "LESSON_NOT_FOUND",
        message: "Lesson not found",
      });
    let evaluation = await db.evaluation
      .create({
        lessonId: lesson.id,
        userId: user.id,
        score: Number(req.body.score),
      })
      .catch((err) => {
        res.send({ code: "CREATION_ERROR", message: err.message });
      });
    return res.send({
      id: evaluation.id,
      user_id: evaluation.userId,
      score: evaluation.score,
    });
  } catch (err) {
    res.send({ code: err.name, message: err.message });
  }
};

async function findUser(id) {
  let user = await db.user.findOne({
    where: { id: Number(id) },
  });
  return user;
}

async function findLesson(id) {
  let lesson = await db.lesson.findOne({
    where: { id: Number(id) },
  });
  return lesson;
}
exports.getLessons = async function (req, res) {
  try {
    let lessons = await db.lesson
      .findAll({
        include: {
          model: db.evaluation,
          as: "evaluations",
          attributes: ["id", "score"],
          include: {
            model: db.user,
            as: "user",
            attributes: ["id", "name", "email"],
          },
        },
      })
      .catch((err) => {
        res.send({ code: "FIND_ERROR", message: err.name });
      });
    return res.send(lessons);
  } catch (err) {
    res.send({ code: err.name, message: err.message });
  }
};
