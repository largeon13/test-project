const db = require("@lib/db");
const validation = require("validator");

exports.addUser = async function (req, res) {
  try {
    let resultValidation = await validation.validateData(req.body, "addUser");
    if (!resultValidation.success) {
      return res.send({
        code: "VALIDATION_ERROR",
        message: resultValidation.error,
      });
    }
    let user_exist = await db.user.findOne({
      where: { email: req.body.email },
    });
    if (user_exist) {
      return res.send({
        code: "USER_ALREADY_EXISTS",
        message: "A user with this email already exists",
      });
    }
    let user = await db.user
      .create({
        name: req.body.name,
        email: req.body.email,
      })
      .catch((err) => {
        res.send({ code: "CREATION_ERROR", message: err.name });
      });
    return res.send(user);
  } catch (err) {
    res.send({ code: err.name, message: err.message });
  }
};

exports.getUsers = async function (req, res) {
  try {
    let users = await db.user.findAll();
    return res.send(users);
  } catch (err) {
    res.send({ code: err.name, message: err.message });
  }
};
