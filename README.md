# TEST PROJECT

## Install on local host

Install dependencies

```
npm install
```

create database

```
sudo -u postgres createdb test_db
```

Run migrations

```
npm run migrate
```

## Usage

Launch

```
npm start
```

swagger: [http://localhost:8080/docs](http://localhost:8080/docs)
