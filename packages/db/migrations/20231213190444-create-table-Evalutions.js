"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("evaluations", {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      score: Sequelize.INTEGER,
      createdAt: Sequelize.DATE,
      userId: Sequelize.INTEGER,
      lessonId: Sequelize.INTEGER,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("evaluations");
  },
};
