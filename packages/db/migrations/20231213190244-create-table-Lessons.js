"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("lessons", {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      name: Sequelize.STRING(100),
      code: Sequelize.STRING(20),
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("lessons");
  },
};
