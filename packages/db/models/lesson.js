"use strict";
module.exports = (sequelize, DataTypes) => {
  const lesson = sequelize.define(
    "lesson",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: DataTypes.STRING(100),
      code: DataTypes.STRING(20),
    },
    {
      createdAt: false,
      updatedAt: false,
    }
  );
  lesson.associate = function (models) {
    lesson.hasMany(models.evaluation, {
      as: "evaluations",
      foreignKey: "lessonId",
    });
  };
  return lesson;
};
