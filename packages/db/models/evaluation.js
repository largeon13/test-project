"use strict";
module.exports = (sequelize, DataTypes) => {
  const evaluation = sequelize.define(
    "evaluation",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      score: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: "user",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      lessonId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: "lesson",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    },
    {
      createdAt: true,
      updatedAt: false,
    }
  );
  evaluation.associate = function (models) {
    evaluation.belongsTo(models.lesson, {
      foreignKey: "lessonId",
      targetKey: "id",
    });
    evaluation.belongsTo(models.user, {
      as: "user",
      foreignKey: "userId",
      targetKey: "id",
    });
  };
  return evaluation;
};
