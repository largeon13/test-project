exports.addUser = {
  type: "object",
  properties: {
    name: { type: "string" },
    email: { type: "string", format: "email" },
  },
  required: ["name", "email"],
};

exports.addLesson = {
  type: "object",
  properties: {
    name: { type: "string" },
    code: { type: "string" },
  },
  required: ["name", "code"],
};

exports.addEvalution = {
  type: "object",
  properties: {
    score: { type: "string" },
    user_id: { type: "string" },
    lesson_id: { type: "string" },
  },
  required: ["score", "user_id", "lesson_id"],
};
